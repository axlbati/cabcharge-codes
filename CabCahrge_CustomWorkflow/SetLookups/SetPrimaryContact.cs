﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetLookups
{
    public sealed class SetPrimaryContact : CodeActivity
    {

        [RequiredArgument]
        [Input("Primary Contact")]
        public InArgument<string> PrimaryContact { get; set; }

        protected override void Execute(CodeActivityContext execContext)
        {
            IWorkflowContext context = execContext.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = execContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.InitiatingUserId);

            //Created GUID
            Guid currentID = context.PrimaryEntityId;

            //Check if Primary Contact exist in Contact Entity
            string primaryContact = this.PrimaryContact.Get(execContext);

            var queryCon = new QueryExpression("contact");
            queryCon.ColumnSet.AddColumns("contactid", "fullname");
            queryCon.Criteria.AddCondition("fullname", ConditionOperator.Equal, primaryContact);
            EntityCollection queryRes2 = service.RetrieveMultiple(queryCon);

            if (queryRes2.Entities.Count > 0)
            { //if Contact Exist

                Guid contactid = queryRes2.Entities.First().GetAttributeValue<Guid>("contactid");

                //Assign Contact to Account Record
                var accountUpdate = new Entity("account");
                accountUpdate.Id = currentID;
                accountUpdate["primarycontactid"] = new EntityReference("contact", contactid);
                service.Update(accountUpdate);
            }
            else if (queryRes2.Entities.Count == 0)
            {

                //Create New Contact
                var newContact = new Entity("contact");
                newContact["lastname"] = primaryContact;
                Guid contactID = service.Create(newContact);

                //Associate Record
                var accountUpdate = new Entity("account");
                accountUpdate.Id = currentID;
                accountUpdate["primarycontactid"] = new EntityReference("contact", contactID);
                service.Update(accountUpdate);
            }
        }
    }
}
