﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetLookups
{
    public sealed class SetDomain : CodeActivity
    {

        [RequiredArgument]
        [Input("Domain")]
        public InArgument<string> Domain { get; set; }

        protected override void Execute(CodeActivityContext execContext)
        {
            IWorkflowContext context = execContext.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = execContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.InitiatingUserId);

            //Created GUID
            Guid currentID = context.PrimaryEntityId;

            //Check if Domain exist in Client Entity
            string domain = this.Domain.Get(execContext);

            var queryAcc = new QueryExpression("account");
            queryAcc.ColumnSet.AddColumns("accountid","name");
            queryAcc.Criteria.AddCondition("name", ConditionOperator.Equal, domain);
            EntityCollection queryRes = service.RetrieveMultiple(queryAcc);

            if (queryRes.Entities.Count > 0)
            { //If Domain Exist
                //Assign Parent Account
                Guid accountid = queryRes.Entities.First().GetAttributeValue<Guid>("accountid");
                string accName = queryRes.Entities.First().GetAttributeValue<string>("name");

                //throw new InvalidPluginExecutionException("Account Name: " + accName + " " + "Accountid: " + accountid);

                

                var accountUpdate = new Entity("account");
                accountUpdate.Id = currentID;
                accountUpdate["parentaccountid"] = new EntityReference("account", accountid);
                service.Update(accountUpdate);
            }
            else if (queryRes.Entities.Count == 0)
            {

                Guid currency = Guid.Parse("f0572126-40fd-e711-a838-000d3ae0e65e");

                //Create New Account
                var newAccount = new Entity("account");
                newAccount["name"] = domain;
                newAccount["transactioncurrencyid"] = new EntityReference("transactioncurrency", currency);
                Guid accountID = service.Create(newAccount);

                //Associate record
                var accountUpdate = new Entity("account");
                accountUpdate.Id = currentID;
                accountUpdate["parentaccountid"] = new EntityReference("account", accountID);
                service.Update(accountUpdate);
            }
        }
    }
}
