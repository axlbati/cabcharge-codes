﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetLookups
{
    public sealed class SetSecondaryContact : CodeActivity
    {

        [RequiredArgument]
        [Input("Secondary Contact")]
        public InArgument<string> SecondaryContact { get; set; }

        protected override void Execute(CodeActivityContext execContext)
        {
            IWorkflowContext context = execContext.GetExtension<IWorkflowContext>();
            IOrganizationServiceFactory serviceFactory = execContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.InitiatingUserId);

            //Created GUID
            Guid currentID = context.PrimaryEntityId;

            //Check if Secondary Contact is exist
            var secContact = this.SecondaryContact.Get(execContext);

            //Query Secondary Contact to Client Entity
            var secContact_query = new QueryExpression("contact");
            secContact_query.ColumnSet.AddColumns("contactid", "fullname");
            secContact_query.Criteria.AddCondition("fullname", ConditionOperator.Equal, secContact);
            EntityCollection secQueryres = service.RetrieveMultiple(secContact_query);

            if (secQueryres.Entities.Count > 0)
            {

                Guid contactid = secQueryres.Entities.First().GetAttributeValue<Guid>("contactid");
                //Associate to the Main
                var accountUpdate = new Entity("account");
                accountUpdate.Id = contactid;
                accountUpdate["cab_secondaryauthorisedid"] = new EntityReference("contact", contactid);
                service.Update(accountUpdate);
            }
            else if (secQueryres.Entities.Count == 0) 
            {
                
                //Create New Contact
                var newContact = new Entity("contact");
                newContact["lastname"] = secContact;
                Guid contactID = service.Create(newContact);

                //Associate Record
                var accountUpdate = new Entity("account");
                accountUpdate.Id = currentID;
                accountUpdate["cab_secondaryauthorisedid"] = new EntityReference("contact", contactID);
                service.Update(accountUpdate);
            }
        }
    }
}
