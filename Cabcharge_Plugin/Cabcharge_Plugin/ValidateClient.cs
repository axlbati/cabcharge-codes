﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cabcharge_Plugin
{
    public class ValidateClient : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            ITracingService tracer = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            
            if (context.MessageName == "Create" && context.PrimaryEntityName == "cab_staging")
            {
                Entity entity = (Entity)context.InputParameters["Target"];

                //Text fields
                string accountnumber = "";
                string cab_name = "";
                string cab_abn = "";
                string cab_address1zippostalcode = "";
                string cab_address2zippostalcode = "";
                string cab_address1street1 = "";
                string cab_address1street2 = "";
                string cab_address1street3 = "";
                string cab_address1street4 = "";
                string cab_apemailforeinvoices = "";
                string cab_casemanager = "";
                string cab_crmmanager = "";
                string cab_currentstatus = "";
                string cab_dateclosed = "";
                string cab_dateopened = "";
                string cab_lastactive = "";
                string cab_docketbookcontact1 = "";
                string cab_docketbookcontact2 = "";
                string cab_docketbookcontactinfo = "";
                string cab_docketbooklabel1 = "";
                string cab_docketbooklabel2 = "";
                string cab_docketbooklabel3 = "";
                string cab_docketbooklabel4 = "";
                string cab_docketbooklabel5 = "";
                string cab_email = "";
                string cab_eticketcontact1 = "";
                string cab_eticketcontact2 = "";
                string cab_eticketcontactinfo = "";
                string cab_eticketlabel1 = "";
                string cab_eticketlabel2 = "";
                string cab_eticketlabel3 = "";
                string cab_eticketlabel4 = "";
                string cab_eticketlabel5 = "";
                string cab_hardcopystatement = "";
                string cab_mainphone = "";
                string cab_telephone2 = "";
                string cab_paymentterms = "";
                string cab_primaryauthorizedcontact = "";
                string cab_secondaryauthorizedcontact = "";
                string cab_groupdomain = "";
                string cab_stmtemail = "";
                string cab_nooffastcardsonaccount = "";
                string cab_noofusersoncabchargeplus = "";
                string cab_p13totaltrips = "";

                //Money Fields
                Money cab_p13docketfare = new Money();
                Money cab_p13eftcardfare = new Money();
                Money cab_p13eticketfare = new Money();
                Money cab_p13flexeticketfare = new Money();
                Money cab_p13servicefee = new Money();
                Money cab_p13total = new Money();
                Money cab_p13totalfare = new Money();

                

                if (entity.Attributes.Contains("cab_accountnumber"))
                {
                    accountnumber = (string)entity.Attributes["cab_accountnumber"];
                }

                if (entity.Attributes.Contains("cab_name"))
                {
                    cab_name = (string)entity.Attributes["cab_name"];
                }

                if (entity.Attributes.Contains("cab_abn"))
                {
                    cab_abn = (string)entity.Attributes["cab_abn"];
                }

                if (entity.Attributes.Contains("cab_address1street1"))
                {
                    cab_address1street1 = (string)entity.Attributes["cab_address1street1"];
                }

                if (entity.Attributes.Contains("cab_address1street2"))
                {
                    cab_address1street2 = (string)entity.Attributes["cab_address1street2"];
                }

                if (entity.Attributes.Contains("cab_address1street3"))
                {
                    cab_address1street3 = (string)entity.Attributes["cab_address1street3"];
                }

                if (entity.Attributes.Contains("cab_address1street4"))
                {
                    cab_address1street4 = (string)entity.Attributes["cab_address1street4"];
                }

                if (entity.Attributes.Contains("cab_address1zippostalcode"))
                {
                    cab_address1zippostalcode = (string)entity.Attributes["cab_address1zippostalcode"];
                }

                if (entity.Attributes.Contains("cab_address2zippostalcode"))
                {
                    cab_address2zippostalcode = (string)entity.Attributes["cab_address2zippostalcode"];
                }

                if (entity.Attributes.Contains("cab_apemailforeinvoices"))
                {
                    cab_apemailforeinvoices = (string)entity.Attributes["cab_apemailforeinvoices"];
                }

                if (entity.Attributes.Contains("cab_casemanager"))
                {
                    cab_casemanager = (string)entity.Attributes["cab_casemanager"];
                }

                if (entity.Attributes.Contains("cab_crmmanager"))
                {
                    cab_crmmanager = (string)entity.Attributes["cab_crmmanager"];
                }

                if (entity.Attributes.Contains("cab_currentstatus"))
                {
                    cab_currentstatus = (string)entity.Attributes["cab_currentstatus"];
                }

                if (entity.Attributes.Contains("cab_dateclosed"))
                {
                    cab_dateclosed = (string)entity.Attributes["cab_dateclosed"];
                }

                if (entity.Attributes.Contains("cab_dateopened"))
                {
                    cab_dateopened = (string)entity.Attributes["cab_dateopened"];
                }

                if (entity.Attributes.Contains("cab_docketbookcontact1"))
                {
                    cab_docketbookcontact1 = (string)entity.Attributes["cab_docketbookcontact1"];
                }

                if (entity.Attributes.Contains("cab_docketbookcontact2"))
                {
                    cab_docketbookcontact2 = (string)entity.Attributes["cab_docketbookcontact2"];
                }

                if (entity.Attributes.Contains("cab_docketbookcontactinfo"))
                {
                    cab_docketbookcontactinfo = (string)entity.Attributes["cab_docketbookcontactinfo"];
                }

                if (entity.Attributes.Contains("cab_docketbooklabel1"))
                {
                    cab_docketbooklabel1 = (string)entity.Attributes["cab_docketbooklabel1"];
                }

                if (entity.Attributes.Contains("cab_docketbooklabel2"))
                {
                    cab_docketbooklabel2 = (string)entity.Attributes["cab_docketbooklabel2"];
                }

                if (entity.Attributes.Contains("cab_docketbooklabel3"))
                {
                    cab_docketbooklabel3 = (string)entity.Attributes["cab_docketbooklabel3"];
                }

                if (entity.Attributes.Contains("cab_docketbooklabel4"))
                {
                    cab_docketbooklabel4 = (string)entity.Attributes["cab_docketbooklabel4"];
                }

                if (entity.Attributes.Contains("cab_docketbooklabel5"))
                {
                    cab_docketbooklabel5 = (string)entity.Attributes["cab_docketbooklabel5"];
                }

                if (entity.Attributes.Contains("cab_email"))
                {
                    cab_email = (string)entity.Attributes["cab_email"];
                }

                if (entity.Attributes.Contains("cab_eticketcontact1"))
                {
                    cab_eticketcontact1 = (string)entity.Attributes["cab_eticketcontact1"];
                }

                if (entity.Attributes.Contains("cab_eticketcontact2"))
                {
                    cab_eticketcontact2 = (string)entity.Attributes["cab_eticketcontact2"];
                }

                if (entity.Attributes.Contains("cab_eticketcontactinfo"))
                {
                    cab_eticketcontactinfo = (string)entity.Attributes["cab_eticketcontactinfo"];
                }

                if (entity.Attributes.Contains("cab_eticketlabel1"))
                {
                    cab_eticketlabel1 = (string)entity.Attributes["cab_eticketlabel1"];
                }

                if (entity.Attributes.Contains("cab_eticketlabel2"))
                {
                    cab_eticketlabel2 = (string)entity.Attributes["cab_eticketlabel2"];
                }

                if (entity.Attributes.Contains("cab_eticketlabel3"))
                {
                    cab_eticketlabel3 = (string)entity.Attributes["cab_eticketlabel3"];
                }

                if (entity.Attributes.Contains("cab_eticketlabel4"))
                {
                    cab_eticketlabel4 = (string)entity.Attributes["cab_eticketlabel4"];
                }

                if (entity.Attributes.Contains("cab_eticketlabel5"))
                {
                    cab_eticketlabel5 = (string)entity.Attributes["cab_eticketlabel5"];
                }

                if (entity.Attributes.Contains("cab_hardcopystatement"))
                {
                    cab_hardcopystatement = (string)entity.Attributes["cab_hardcopystatement"];
                }

                if (entity.Attributes.Contains("cab_lastactive"))
                {
                    cab_lastactive = (string)entity.Attributes["cab_lastactive"];
                }

                if (entity.Attributes.Contains("cab_mainphone"))
                {
                    cab_mainphone = (string)entity.Attributes["cab_mainphone"];
                }
                if (entity.Attributes.Contains("cab_paymentterms"))
                {
                    cab_paymentterms = (string)entity.Attributes["cab_paymentterms"];
                }

                if (entity.Attributes.Contains("cab_secondaryauthorizedcontact"))
                {
                    cab_secondaryauthorizedcontact = (string)entity.Attributes["cab_secondaryauthorizedcontact"];
                }

                if (entity.Attributes.Contains("cab_primaryauthorizedcontact"))
                {
                    cab_primaryauthorizedcontact = (string)entity.Attributes["cab_primaryauthorizedcontact"];
                }

                if (entity.Attributes.Contains("cab_telephone2"))
                {
                    cab_telephone2 = (string)entity.Attributes["cab_telephone2"];
                }

                if (entity.Attributes.Contains("cab_groupdomain"))
                {
                    cab_groupdomain = (string)entity.Attributes["cab_groupdomain"];
                }

                if (entity.Attributes.Contains("cab_stmtemail"))
                {
                    cab_stmtemail = (string)entity.Attributes["cab_stmtemail"];
                }

                if (entity.Attributes.Contains("cab_nooffastcardonaccounttext"))
                {
                    cab_nooffastcardsonaccount = (string)entity.Attributes["cab_nooffastcardonaccounttext"];
                }

                if (entity.Attributes.Contains("cab_noofusersoncabchargeplustext"))
                {
                    cab_noofusersoncabchargeplus = (string)entity.Attributes["cab_noofusersoncabchargeplustext"];
                }

                if (entity.Attributes.Contains("cab_p13totaltrips"))
                {
                    cab_p13totaltrips = (string)entity.Attributes["cab_p13totaltrips"];
                }

                if (string.IsNullOrEmpty(entity.GetAttributeValue<string>("cab_docketfaretext")))
                {
                    cab_p13docketfare.Value = Convert.ToDecimal("0");
                }
                else {
                    cab_p13docketfare.Value = Convert.ToDecimal(entity.Attributes["cab_docketfaretext"]);
                }

                if (string.IsNullOrEmpty(entity.GetAttributeValue<string>("cab_p13eftcardfare")))
                {
                    cab_p13eftcardfare.Value = Convert.ToDecimal("0");
                }
                else {
                    cab_p13eftcardfare.Value = Convert.ToDecimal(entity.Attributes["cab_p13eftcardfare"]);
                }

                if (string.IsNullOrEmpty(entity.GetAttributeValue<string>("cab_p13eticketfare")))
                {
                    cab_p13eticketfare.Value = Convert.ToDecimal("0");
                }   
                else {
                    cab_p13eticketfare.Value = Convert.ToDecimal(entity.Attributes["cab_p13eticketfare"]);
                }

                if (string.IsNullOrEmpty(entity.GetAttributeValue<string>("cab_p13flexeticketfare")))
                {
                    cab_p13flexeticketfare.Value = Convert.ToDecimal("0");
                }
                else {
                    cab_p13flexeticketfare.Value = Convert.ToDecimal(entity.Attributes["cab_p13flexeticketfare"]);
                }

                if (string.IsNullOrEmpty(entity.GetAttributeValue<string>("cab_p13servicefee")))
                {
                    cab_p13servicefee.Value = Convert.ToDecimal("0");
                }
                else {
                    cab_p13servicefee.Value = Convert.ToDecimal(entity.Attributes["cab_p13servicefee"]);
                }

                if (string.IsNullOrEmpty(entity.GetAttributeValue<string>("cab_p13total")))
                {
                    cab_p13total.Value = Convert.ToDecimal("0");
                }
                else {
                    cab_p13total.Value = Convert.ToDecimal(entity.Attributes["cab_p13total"]);
                }

                if (string.IsNullOrEmpty(entity.GetAttributeValue<string>("cab_p13totalfare")))
                {
                    cab_p13totalfare.Value = Convert.ToDecimal("0");
                }
                else {
                    cab_p13totalfare.Value = Convert.ToDecimal(entity.Attributes["cab_p13totalfare"]);
                }


                //throw new InvalidPluginExecutionException("Money: " + cab_p13docketfare);

                Guid onwerid = Guid.Parse("6a0e55dc-1a11-e811-a83b-000d3ae0e4ef");
                Guid currencyid = Guid.Parse("f0572126-40fd-e711-a838-000d3ae0e65e");

                var queryClient = new QueryExpression("account");
                queryClient.ColumnSet.AddColumns("accountid", "accountnumber");
                queryClient.Criteria.AddCondition("accountnumber", ConditionOperator.Equal, accountnumber);
                EntityCollection clientRes = service.RetrieveMultiple(queryClient);

                if (clientRes.Entities.Count > 0)
                { // Account Number is exist

                    Guid accountid = clientRes.Entities.First().GetAttributeValue<Guid>("accountid");
                    //throw new InvalidPluginExecutionException("Account Number: " + accountnumber);

                    //Update Process
                    Entity updateclient = new Entity("account");
                    updateclient.Id = accountid;
                    updateclient["accountnumber"] = accountnumber.ToString();
                    updateclient["name"] = cab_name.ToString();
                    updateclient["cab_abn"] = cab_abn;
                    updateclient["address1_postalcode"] = cab_address1zippostalcode;
                    updateclient["address2_postalcode"] = cab_address2zippostalcode;
                    updateclient["address1_line1"] = cab_address1street1;
                    updateclient["address1_line2"] = cab_address1street2;
                    updateclient["address1_line3"] = cab_address1street3;
                    updateclient["cab_address4"] = cab_address1street4;
                    updateclient["emailaddress2"] = cab_apemailforeinvoices;
                    updateclient["cab_casemanager"] = cab_casemanager;
                    updateclient["cab_crmmanager"] = cab_crmmanager;
                    updateclient["cab_currentstatus"] = cab_currentstatus;
                    updateclient["cab_dateclosedtext"] = cab_dateclosed;
                    updateclient["cab_dateopenedtext"] = cab_dateopened;
                    updateclient["cab_lastactivetext"] = cab_lastactive;
                    updateclient["cab_docketbookcontact1"] = cab_docketbookcontact1;
                    updateclient["cab_docketbookcontact2"] = cab_docketbookcontact2;
                    updateclient["cab_docketbookcontactinfo"] = cab_docketbookcontactinfo;
                    updateclient["cab_docketbooklabel1"] = cab_docketbooklabel1;
                    updateclient["cab_docketbooklabel2"] = cab_docketbooklabel2;
                    updateclient["cab_docketbooklabel3"] = cab_docketbooklabel3;
                    updateclient["cab_docketbooklabel4"] = cab_docketbooklabel4;
                    updateclient["cab_docketbooklabel5"] = cab_docketbooklabel5;
                    updateclient["emailaddress1"] = cab_email;
                    updateclient["cab_eticketcontact1"] = cab_eticketcontact1;
                    updateclient["cab_eticketcontact2"] = cab_eticketcontact2;
                    updateclient["cab_eticketcontactinfo"] = cab_eticketcontactinfo;
                    updateclient["cab_eticketlabel1"] = cab_eticketlabel1;
                    updateclient["cab_eticketlabel2"] = cab_eticketlabel2;
                    updateclient["cab_eticketlabel3"] = cab_eticketlabel3;
                    updateclient["cab_eticketlabel4"] = cab_eticketlabel4;
                    updateclient["cab_eticketlabel5"] = cab_eticketlabel5;
                    updateclient["cab_hardcopystatement"] = cab_hardcopystatement;
                    updateclient["telephone1"] = cab_mainphone;
                    updateclient["telephone2"] = cab_telephone2;
                    updateclient["cab_p13docketfare"] = cab_p13docketfare;
                    updateclient["cab_p13"] = cab_p13eftcardfare;
                    updateclient["cab_p13eticketfare"] = cab_p13eticketfare;
                    updateclient["cab_p13flexeticketfare"] = cab_p13flexeticketfare;
                    updateclient["cab_p13servicefee"] = cab_p13servicefee;
                    updateclient["cab_p13total"] = cab_p13total;
                    updateclient["cab_p13totalfare"] = cab_p13totalfare;
                    updateclient["cab_p13totaltrips"] = Convert.ToInt32(cab_p13totaltrips);
                    updateclient["cab_noofusersoncabchargeplus"] = Convert.ToInt32(cab_noofusersoncabchargeplus);
                    updateclient["cab_nooffastcardsonaccount"] = Convert.ToInt32(cab_nooffastcardsonaccount);
                    updateclient["cab_paymentterms"] = cab_paymentterms;
                    updateclient["cab_stmtemailtext"] = cab_stmtemail;
                    updateclient["cab_primarycontacttext"] = cab_primaryauthorizedcontact;
                    updateclient["cab_secondarycontacttext"] = cab_secondaryauthorizedcontact;
                    updateclient["cab_groupdomaintext"] = cab_groupdomain;
                    updateclient["ownerid"] = new EntityReference("systemuser", onwerid);
                    updateclient["transactioncurrencyid"] = new EntityReference("transactioncurrency", currencyid);
                    service.Update(updateclient);

                    entity.Attributes["cab_stagingstatus"] = true;
                }
                else if (clientRes.Entities.Count == 0)
                { // Create new Client Record

                    //throw new InvalidPluginExecutionException("Money: " + cab_p13docketfare);
                    //throw new InvalidPluginExecutionException("Account Number: " + accountnumber);

                    Entity createClient = new Entity("account");
                    createClient["accountnumber"] = accountnumber.ToString();
                    createClient["name"] = cab_name.ToString();
                    createClient["cab_abn"] = cab_abn;
                    createClient["address1_postalcode"] = cab_address1zippostalcode;
                    createClient["address2_postalcode"] = cab_address2zippostalcode;
                    createClient["address1_line1"] = cab_address1street1;
                    createClient["address1_line2"] = cab_address1street2;
                    createClient["address1_line3"] = cab_address1street3;
                    createClient["cab_address4"] = cab_address1street4;
                    createClient["emailaddress2"] = cab_apemailforeinvoices;
                    createClient["cab_casemanager"] = cab_casemanager;
                    createClient["cab_crmmanager"] = cab_crmmanager;
                    createClient["cab_currentstatus"] = cab_currentstatus;
                    createClient["cab_dateclosedtext"] = cab_dateclosed;
                    createClient["cab_dateopenedtext"] = cab_dateopened;
                    createClient["cab_lastactivetext"] = cab_lastactive;
                    createClient["cab_docketbookcontact1"] = cab_docketbookcontact1;
                    createClient["cab_docketbookcontact2"] = cab_docketbookcontact2;
                    createClient["cab_docketbookcontactinfo"] = cab_docketbookcontactinfo;
                    createClient["cab_docketbooklabel1"] = cab_docketbooklabel1;
                    createClient["cab_docketbooklabel2"] = cab_docketbooklabel2;
                    createClient["cab_docketbooklabel3"] = cab_docketbooklabel3;
                    createClient["cab_docketbooklabel4"] = cab_docketbooklabel4;
                    createClient["cab_docketbooklabel5"] = cab_docketbooklabel5;
                    createClient["emailaddress1"] = cab_email;
                    createClient["cab_eticketcontact1"] = cab_eticketcontact1;
                    createClient["cab_eticketcontact2"] = cab_eticketcontact2;
                    createClient["cab_eticketcontactinfo"] = cab_eticketcontactinfo;
                    createClient["cab_eticketlabel1"] = cab_eticketlabel1;
                    createClient["cab_eticketlabel2"] = cab_eticketlabel2;
                    createClient["cab_eticketlabel3"] = cab_eticketlabel3;
                    createClient["cab_eticketlabel4"] = cab_eticketlabel4;
                    createClient["cab_eticketlabel5"] = cab_eticketlabel5;
                    createClient["cab_hardcopystatement"] = cab_hardcopystatement;
                    createClient["telephone1"] = cab_mainphone;
                    createClient["telephone2"] = cab_telephone2;
                    createClient["cab_p13docketfare"] = cab_p13docketfare;
                    createClient["cab_p13"] = cab_p13eftcardfare;
                    createClient["cab_p13eticketfare"] = cab_p13eticketfare;
                    createClient["cab_p13flexeticketfare"] = cab_p13flexeticketfare;
                    createClient["cab_p13servicefee"] = cab_p13servicefee;
                    createClient["cab_p13total"] = cab_p13total;
                    createClient["cab_p13totalfare"] = cab_p13totalfare;
                    createClient["cab_p13totaltrips"] = Convert.ToInt32(cab_p13totaltrips);
                    createClient["cab_noofusersoncabchargeplus"] = Convert.ToInt32(cab_noofusersoncabchargeplus);
                    createClient["cab_nooffastcardsonaccount"] = Convert.ToInt32(cab_nooffastcardsonaccount);
                    createClient["cab_paymentterms"] = cab_paymentterms;
                    createClient["cab_stmtemailtext"] = cab_stmtemail;
                    createClient["cab_primarycontacttext"] = cab_primaryauthorizedcontact;
                    createClient["cab_secondarycontacttext"] = cab_secondaryauthorizedcontact;
                    createClient["cab_groupdomaintext"] = cab_groupdomain;
                    createClient["ownerid"] = new EntityReference("systemuser", onwerid);
                    createClient["transactioncurrencyid"] = new EntityReference("transactioncurrency", currencyid);
                    service.Create(createClient);

                    entity.Attributes["cab_stagingstatus"] = true;
                }
            }
        }
    }
}
